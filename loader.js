function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var inviteCodeGet = getUrlParameter('inviteCode');
if (inviteCodeGet) {
    createCookie('_inviteCode', inviteCodeGet, 3);
    console.log(inviteCodeGet);
}

var testenv = getUrlParameter('test');
var geoParam;
var is_mobile = false;


function loadHTML() {
	 console.log("Loading loadHTML() function...");

    if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        //Source: http://stackoverflow.com/questions/3514784/what-is-the-best-way-to-detect-a-mobile-device-in-jquery
        // is_mobile = true;
    }


    // prepare geo data for submit because there is no easy way to prevent default event and query geo-ip before submission
    console.log("calling geo-ip");
    $.getJSON('https://geoip-db.com/json/geoip.php?jsonp=?').done(function(location) {
        console.log("done");
        geoParam = '&Country=' + location.country_name +
            '&State=' + location.state +
            '&City=' + location.city +
            '&Zip=' + location.postal +
            '&Lat=' + location.latitude +
            '&Long=' + location.longitude +
            '&IP=' + location.IPv4;
    })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("logging user");
            logUser();
        });

    // invert the Facebook and email signups
    jQuery("#new_person").after(jQuery(".signup-horizontal-line"), jQuery("#fb-login"));
    
    // move deposit field right after the price
    if ($('#new_listing_form').length > 0) {
        var label_desc = $("label[for='listing_description']");
        var label_deposit = $("label[for='custom_fields_22185']");
        var info_deposit = '<div class="info-text-container"><div class="info-text-icon"><i class="ss-info"></i></div><div class="info-text-content"><p class="fr">Une caution à  remettre en cash représente une garantie en cas de dommage. Elle ne peut excéder la valeur résiduelle de l\'appareil.</p><p class="en">A deposit in cash consitutes a guarantee in case of dammage. It cannot exceed the residual value of the power tool.</p></div></div>';
        var suffix_currency = '<div class="currency-selector">€<input value="EUR" type="hidden" name="listing[currency]" id="listing_currency" /></div>';
        label_desc.before(label_deposit, jQuery("#custom_fields_22185"), suffix_currency, info_deposit);
        jQuery("#custom_fields_22185").addCLass("price-field"); //does not work
    }
}

function changeDOM() {
	console.log("Loading changeDOM() function..."+document.documentElement.lang);
	$(".marketplace-lander-content-title").remove();
	// Is the data-reactid a robust identifier?
	//$("a[data-reactid='54']").attr('href', 'https://toolsity.com/fr/');
	//$("a[data-reactid='53']").attr('href', 'https://toolsity.com/');
	// change the other links to the toolsity.com variant
	if (document.documentElement.lang == "fr") {
		$("a[href='/']:eq(2)").attr('href', 'https://toolsity.com/fr/');
		$("a[href='/fr/infos/about']").attr('href', 'https://toolsity.com/fr/a-propos/');
		$("a[href='/fr/user_feedbacks/new']").attr('href', 'https://toolsity.com/fr/contact/');
		// the default site (set to french) does not contain the fr/ prefix
		$("a[href='/infos/about']").attr('href', 'https://toolsity.com/fr/a-propos/');
		$("a[href='/user_feedbacks/new']").attr('href', 'https://toolsity.com/fr/contact/');
	}else{
		$("a[href='/']").eq(2).attr('href', 'https://toolsity.com/');
		$("a[href='/en/infos/about']").attr('href', 'https://toolsity.com/about-us/');
		$("a[href='/en/user_feedbacks/new']").attr('href', 'https://toolsity.com/contact/');
	}
	$("a[href='/fr/invitations/new']").remove();
	$("a[href='/en/invitations/new']").remove();
	$("a[href='/invitations/new']").remove();
        
	// add description to phone number field
	if ( $("#person_phone_number").length > 0 ) {
		var phone_description = '<span class="alert-box-icon"><i class="ss-info icon-fix"></i></span><small>Votre numéro de téléphone n&#39;est visible qu&#39;à l&#39;équipe Toolsity et ne sera utilisé qu&#39;en cas d&#39;urgence. En indiquant votre numéro, vous agmenter le niveau de service que nous pouvons vous offrir.</small>';
		$(phone_description).insertBefore($("#person_phone_number"));
		
		$("small:contains('Berlin')").html("Vous pouvez entrer votre adresse ou simplement une ville ou un code postal. Une addresse exacte permet de placer de manière précise les annonces sur la carte. L'adresse n'est visible qu'aux membres inscrits");
	}
	
	// add description to follow button
	console.log($(".follow-button-container").length);
	if ( $(".follow-button-container").length > 0 ) {
		if (document.documentElement.lang == "fr") {
			var follow_description = '<span class="alert-box-icon"><i class="ss-info icon-fix"></i></span><small>En suivant cette personne, vos receverez une notification à chaque nouvelle annonce qu&#39;elle place. La personne ne sera pas avertie que vous la suivez.</small>';
		}else{
			var follow_description = '<span class="alert-box-icon"><i class="ss-info icon-fix"></i></span><small>With this feature, you will receive email notifications each time the user posts a new listing. The user will not receive a notification when you start to follow him/her.</small>';			
		}
		$(follow_description).insertAfter($(".follow-button-container"));
	}
	
}

function registerUser(event) {
	console.log("Preventing default event handler...");

	var userLang = window.navigator.userLanguage || window.navigator.language;
	var code;
	if (readCookie('_inviteCode')) {
		code = readCookie('_inviteCode');
	} else {
		code = "n.a.";
	}
	var parameters = 'Email=' + $("#person_email").val() +
		'&User=' + $("#person_username1").val() +
		'&Code=' + code.toUpperCase() +
		'&PageLang=' + document.documentElement.lang +
		'&BrowserLang=' + userLang +
		'&ColorDepth=' + window.screen.colorDepth +
		'&BrowserName=' + navigator.appName +
		'&BrowserVersion=' + navigator.appVersion +
		'&BrowserCodeName=' + navigator.appCodeName +
		'&Platform=' + navigator.platform +
		'&ScreenWidth=' + window.screen.width +
		'&ScreenHeight=' + window.screen.height +
		'&AvailWidth=' + window.screen.availWidth +
		'&AvailHeight=' + window.screen.availHeight;

	request = $.ajax({
		url: "https://script.google.com/macros/s/AKfycbzyMSvOpJVyYSkK42FPpWPZztoz4I2DNWpJKdNsL9Do-DuRvURZ/exec",
		type: "GET",
		data: parameters + geoParam
	});
}

function logUser() {
	console.log("Logging user...");

	var userLang = window.navigator.userLanguage || window.navigator.language;
	var userID;
	var railsContext = $("#js-react-on-rails-context").attr("data-rails-context");
	var arrayTestList = [];
	arrayTestList = JSON.parse(railsContext);

	if (arrayTestList["loggedInUsername"] == null) {
		userID = "visitor";
	} else {
		userID = arrayTestList["loggedInUsername"];
	}

	var parameters = 'Email=' + "n/a" +
		'&User=' + userID +
		'&Code=' + window.location.pathname +
		'&PageLang=' + document.documentElement.lang +
		'&BrowserLang=' + userLang +
		'&ColorDepth=' + window.screen.colorDepth +
		'&BrowserName=' + navigator.appName +
		'&BrowserVersion=' + navigator.appVersion +
		'&BrowserCodeName=' + navigator.appCodeName +
		'&Platform=' + navigator.platform +
		'&ScreenWidth=' + window.screen.width +
		'&ScreenHeight=' + window.screen.height +
		'&AvailWidth=' + window.screen.availWidth +
		'&AvailHeight=' + window.screen.availHeight;

	request = $.ajax({
		url: "https://script.google.com/macros/s/AKfycbzyMSvOpJVyYSkK42FPpWPZztoz4I2DNWpJKdNsL9Do-DuRvURZ/exec",
		type: "GET",
		data: parameters + geoParam
	});

}
	
$(document).ready(function() {
    /** Disable map details for visitors **/
	changeDOM();
	console.log("checking if user logged in...");
	var login_state = $('a[href*="login"]');
	if (login_state.length != 0) {
		console.log("Removing map");
		if (document.documentElement.lang == "fr") {
			$("<div class='map-overlay'>Veuillez <a class='text-part' href='https://toolsity.sharetribe.com/fr/login'>vous connecter</a> pour afficher les détails de localisation...</div>").css({
				position: "absolute",
				width: "100%",
				height: "100%",
				top: 0,
				opacity: 0,
				left: 0,
				background: "white"
			}).appendTo($("#googlemap").css("position", "relative"));
		}else{
			$("<div class='map-overlay'>Please <a class='text-part' href='https://toolsity.sharetribe.com/en/login'>log in</a> to see the location details...</div>").css({
				position: "absolute",
				width: "100%",
				height: "100%",
				top: 0,
				opacity: 0,
				left: 0,
				background: "white"
			}).appendTo($("#googlemap").css("position", "relative"));			
		}

		$("div.map-overlay").hover(function(){
			$(this).css('opacity', '0.9');
		 }, function() {
			$(this).css('opacity', '0');
		 });

		$(".map-link").remove();
	}

    /** Disable map details for visitors END **/

    /** Contact TAWK **/

    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/59482902e9c6d324a473637a/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();

    /** Contact TAWK END **/

    /** Load FOOTER **/

    if (document.documentElement.lang == "fr") {
        $('<div class="container-fluid footer-wrap"><div class="row"><div class="container footer-container"><div class="row"><div class="col-sm-3"><h4>Toolsity</h4><p>Toolsity est une place de marché pour la location des outils entre particuliers. Sa mission est de permettre au potentiels loueurs et locataires de se rencontrer et de structurer la transaction de location. Toolsity propose une liste claire des annonces par catégorie et par emplacement.</p></div><div class="col-sm-3 footer-pcol"><h4>Info</h4><ul><li><a href="https://toolsity.com/fr/a-propos/">Apropos de nous</a></li><li><a href="https://toolsity.com/fr/comment-ca-marche/">Comment ça marche</a></li></ul></div><div class="col-sm-3 footer-pcol"><h4>Service</h4><ul><li><a href="https://toolsity.com/fr/contact/">Contact</a></li><li><a href="https://toolsity.com/fr/vie-privee/">Vie privée</a></li><li><a href="https://toolsity.com/fr/conditions-dutilisation/">Conditions d\'utilisation</a></li></ul></div><div class="col-sm-3 footer-pcol"><h4>Rejoingez-nous</h4><ul class="social-list"><li><a href="https://www.facebook.com/toolsity" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x green"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span> Facebook</a></li><li><a href="http://www.youtube.com/channel/UCjHyMkC2Dht_NAMLK5UI_GQ" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x green"></i><i class="fa fa-youtube fa-stack-1x fa-inverse"></i></span> Youtube</a></li></ul></div></div></div></div></div>').insertAfter('.page-content');
    } else {
        $('<div class="container-fluid footer-wrap"><div class="row"><div class="container footer-container"><div class="row"><div class="col-sm-3"><h4>Toolsity</h4><p>Toolsity is a marketplace for sharing power tools in urban areas. Its mission is to allow potential lenders and borrowers to meet on the platform and to support the rental transaction. Toolsity provides a clear listing of availablepower tools per category and per location.</p></div><div class="col-sm-3 footer-pcol"><h4>About</h4><ul><li><a href="https://toolsity.com/about-us/">About us</a></li><li><a href="https://toolsity.com/how-it-works/">How it works</a></li></ul></div><div class="col-sm-3 footer-pcol"><h4>Service</h4><ul><li><a href="https://toolsity.com/contact/">Contact</a></li><li><a href="https://toolsity.com/privacy-policy/">Privacy Policy</a></li><li><a href="https://toolsity.com/terms/">Terms of use</a></li></ul></div><div class="col-sm-3 footer-pcol"><h4>Join us</h4><ul class="social-list"><li><a href="https://www.facebook.com/toolsity" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x green"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span> Facebook</a></li><li><a href="http://www.youtube.com/channel/UCjHyMkC2Dht_NAMLK5UI_GQ" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x green"></i><i class="fa fa-youtube fa-stack-1x fa-inverse"></i></span> Youtube</a></li></ul></div></div></div></div></div>').insertAfter('.page-content');
    }

    /** Load FOOTER END**/

    /** Load AUTCOMPLETE **/

    console.log("Loading autocomplete script...");

    var autocomplete1 = new autoComplete({
        selector: '.SearchBar__keywordInput__2HTav',
        minChars: 1,
        source: function(term, suggest) {
            term = term.toLowerCase();
            var choices = ["Ponceuse excentrique", "Karcher K3150 K3.150 240V Pressure Washer with 120 Bar", "Coupe bordure", "Echelle pliante", "Aspirateur Metabo ASA 1202", "Makita GA5030 Meuleuse Ã˜ 125 mm 720 W", "makita hr2470ft", "Makita BO6030", "Makita  visseuse/perceuse 6281D", "DÃ©capeur thermique Bosch Ghg 660", "Makita JR3050T", "Scie sauteuse Metabo", "Fein multimaster.", "Bosch Perceuse-visseuse Ã  percussion sans fil PSB 1", "Agrafeuse sans Fil PTK 3,6 LI Bosch", "Bosch Visseuse sans fil IXO V", "Ponceuse Bosch PSM 200 AES", "Scie sauteuse Bosch PST 800 PEL", "Nettoyeur haute pression - KÃ¤rcher", "Bosch GCL-2 15 Laser lignes et points", "Table de sciage", "Visseuses sans fil Bosch", "Coupe Carrelage", "Visseuse Bosch mini", "Perceuse Nupower", "Scie sauteuse Nupower", "TronÃ§onneuse Ã©lectrique Makita 35cm", "Echelle pliante aluminium 4m (4x1m)", "Tondeuse", "Multifonctions Bosch PMF 190 E", "Perceuse Foreuse Visseuse SKIL", "Scie / presse coupante pour stratifiÃ©", "Visseuse sans fil Bosch PSR 1440 LI-2", "Scie circulaire Bosch PKS 55 A", "Niveau laser Bosch PLL 360", "Niveau 2m", "Kit Burins et forets SDS Plus Makita", "Rabot a main", "Scie Cloche", "Agrafeuse Rapid", "Scie Ã  TronÃ§onner", "Meuleuse d'angle Xceed", "Ponceuse multifonction 4 en 1 Black & Decker", "Outil multifonctions Worx", "DÃ©fonceuse Bosch", "Marteau perforateur- Foreuse Bosch PBH", "Perceuse visseuse Bosch Professional GSR 18 V-LI", "Scie sauteuse PST 700 E", "Ponceuse multi 4 en 1 AutoselectÂ® + 6 accs", "Perceuse Bosch", "Visseuses DeWalt", "Scies circulaires Festool avec rail", "Set de dÃ©molition", "HÃ©lice/mÃ©langeur pour perceuse", "Carotte/cloche de perforage", "Multifonction 300W - Black & Decker", "Perforateur pneumatique 850W - 2.4 J - Black&Decker", "Scie sauteuse Bosch PST 700 E", "Perceuse Bosch 850-2 RE", "Ponceuse excentrique Bosch PEX 300 AE", "Meuleuse angulaire Bosch GWS 7-125"];
            var suggestions = [];
            for (i = 0; i < choices.length; i++)
                if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
            suggest(suggestions);
        }
    });

    /** Load AUTCOMPLETE END**/
});
